/*
Copyright © 2022 Cobalt <cobalt@cobalt.rocks>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package core

import (
	"fmt"
	"os"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var Logger *zap.SugaredLogger

// TODO: Add configuration options for logging - See https://pkg.go.dev/go.uber.org/zap#example-package-BasicConfiguration as a starting point
func SetupLogger() {
	if viper.GetString("mode") == "production" {
		if ProdLogger, err := zap.NewProduction(); err != nil {
			fmt.Printf("Failed to initialize production logger: %v\n", err)
			os.Exit(1)
		} else {
			Logger = ProdLogger.Sugar()
		}
	} else {
		if DevLogger, err := zap.NewDevelopment(); err != nil {
			fmt.Printf("Failed to initialize production logger: %v\n", err)
			os.Exit(1)
		} else {
			Logger = DevLogger.Sugar()
		}
	}
}

// ZapLogger is an example of echo middleware that logs requests using logger "zap"
func LoggerMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// start timer and handle request
		start := time.Now()

		if err := next(c); err != nil {
			c.Error(err)
		}

		// extract request and response information and save to zap field struct
		req, res := c.Request(), c.Response()
		id := req.Header.Get(echo.HeaderXRequestID)
		if id == "" {
			id = res.Header().Get(echo.HeaderXRequestID)
		}

		fields := []zapcore.Field{
			zap.Int("status", res.Status),
			zap.String("latency", time.Since(start).String()),
			zap.String("id", id),
			zap.String("method", req.Method),
			zap.String("uri", req.RequestURI),
			zap.String("host", req.Host),
			zap.String("remote_ip", c.RealIP()),
		}

		// determine level by status and log over Global Logger instance
		n := res.Status
		switch {
		case n >= 500:
			Logger.Error(fields)
		case n >= 400:
			Logger.Warn(fields)
		default:
			Logger.Info(fields)
		}

		return nil
	}

}
