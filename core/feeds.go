/*
Copyright © 2022 Cobalt <cobalt@cobalt.rocks>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package core

import (
	"io"
	"net/http"
	"os"
	"time"

	ical "github.com/arran4/golang-ical"
	"github.com/spf13/viper"
)

type Feed struct {
	Description string
	Url         string
	Type        string
}

type FeedCollection = map[string]Feed

func mapFeedCollection(source map[string]interface{}) FeedCollection {
	// create new FeedCollection by checking type assertions of each source element
	collection := make(FeedCollection, len(source))

	for title, uncheckedData := range source {
		if data, ok := uncheckedData.(map[string]interface{}); !ok {
			Logger.Fatalf("Could not deserialize value of feed title '%s'", title)
		} else {
			// check fields and assure all fields are strings and the type is valid
			feed := Feed{}

			if description, ok := data["description"]; ok {
				if description, ok := description.(string); ok {
					feed.Description = description
				} else {
					Logger.Fatalf("Feed title '%s' description field needs to be a string", title)
				}
			} else {
				Logger.Fatalf("Feed title '%s' missed field description", title)
			}

			if data_type, ok := data["type"]; ok {
				if data_type, ok := data_type.(string); ok && (data_type == "file" || data_type == "web") {
					if url, ok := data["url"]; ok {
						if url, ok := url.(string); ok {
							feed.Url = url
							feed.Type = data_type
						} else {
							Logger.Fatalf("Feed title '%s' url field needs to be a string", title)
						}
					} else {
						Logger.Fatalf("Feed title '%s' missed field url", title)
					}
				} else {
					Logger.Fatalf("Feed title '%s' had invalid field type. only 'web' and 'file' are allowed", title)
				}
			} else {
				Logger.Fatalf("Feed title '%s' missed field type", title)
			}

			// add to cleanly typed collection
			collection[title] = feed
		}
	}

	return collection
}

func RegisterFeeds(dryRun bool) {
	// Load feeds into map
	data := mapFeedCollection(viper.GetStringMap("feeds"))

	// list of ids of all processed	calendars
	feeds := make([]uint, 0, len(data))

	// counter for updated and created entries
	updated, created := 0, 0

	// update feeds in the db
	for title, data := range data {
		// fetch calendar entry from the database
		var calendar Calendar

		if dryRun {
			result := DB.Where(&Calendar{Title: title}).Limit(1).Find(&calendar)

			if result.RowsAffected == 0 {
				Logger.Debugf("Failed to query database (can't add new entries in dry run mode - skipping)")
				continue
			} else if result.Error != nil {
				Logger.Fatalf("Failed to query database for a calendar: %s", result.Error)
			}
		} else {
			// fetch entry and create if not found
			if result := DB.Where(&Calendar{Title: title}).Limit(1).Find(&calendar); result.RowsAffected == 0 {
				calendar.Description = data.Description
				calendar.Url = data.Url
				calendar.Title = title
				calendar.Type = data.Type
				DB.Save(&calendar)
				feeds = append(feeds, calendar.ID)
				created++
				continue
			} else if result.Error != nil {
				Logger.Fatalf("Failed to query database for a calendar: %s", result.Error)
			}
		}

		// add title to feeds collection
		feeds = append(feeds, calendar.ID)

		// compare feed entry against database entry
		if calendar.Description != data.Description {
			if calendar.Url != data.Url {
				if dryRun {
					Logger.Debugf("Updating calendar '%s' with description '%s' and url '%s'", title, data.Description, data.Url)
				} else {
					DB.Where(&calendar).Update("url", data.Url)
					DB.Where(&calendar).Update("description", data.Description)
				}
			} else {
				if dryRun {
					Logger.Debugf("Updating calendar '%s' with description '%s'", title, data.Description)
				} else {
					DB.Where(&calendar).Update("description", data.Description)
				}
			}

			updated++
		}
	}

	// remove all feeds not found in file
	if dryRun {
		Logger.Infof("Removing all feeds except for '%s' and updated %d feeds", feeds, updated)
	} else {
		DB.Not(feeds).Delete(&Calendar{})
		Logger.Infof("Removed %d unreferenced feeds, updated %d and created %d feeds", DB.RowsAffected, updated, created)
	}
}

// get feed and parse as ical feed
func fetchFeed(url, feed_type string) (*ical.Calendar, error) {
	var reader io.Reader

	if feed_type == "web" {
		if resp, err := http.Get(url); err != nil || resp.StatusCode >= 400 {
			return nil, err
		} else {
			reader = resp.Body
		}
	} else if feed_type == "text" {
		if file, err := os.Open(url); err != nil {
			return nil, err
		} else {
			reader = file
		}
	}

	if cal, err := ical.ParseCalendar(reader); err == nil {
		return cal, nil
	} else {
		return nil, err
	}
}

func FetchFeeds() {
	// TODO: Finish implementing fetching logic - Only thing left is the hanling of possible id conflicts
	timeout := time.Duration(viper.GetUint("refresh")) * time.Second
	refresh := time.Second * time.Duration(viper.GetInt64("refresh"))

	// register feeds at startup
	RegisterFeeds(false)

	// this code is intentionally single threaded to avoid writing conflicts with sqlite
	for {
		Logger.Infof("Feed fetching cycle started at %s", time.Now().UTC().Format(time.UnixDate))

		// below might be optimized with channels
		var feeds []Calendar
		feedResult := DB.Find(&feeds)

		if feedResult.Error != nil {
			Logger.Fatalf("Failed to fetch calendars from the database: %s", feedResult.Error)
		}

		if feedResult.RowsAffected == 0 {
			Logger.Info("No feeds were found in the database, fetching cycle skipped")
		} else {
			for idx, feed := range feeds {
				Logger.Infof("Refreshing feeds %d/%d", idx+1, len(feeds))

				// load calendar from database
				var calendar Calendar

				if result := DB.FirstOrInit(
					&calendar,
					&Calendar{Title: feed.Title, Description: feed.Description, Url: feed.Url, Type: feed.Type},
				); result.Error != nil {
					Logger.Error("Failed to run calendar [%s] fetching or creating query against the database: %s", feed.Title, result.Error)
					continue
				}

				// skip feeds if their refresh period hasn't been hit yet
				if time.Now().UTC().Before(calendar.LastRefresh.Add(refresh)) {
					continue
				}

				// fetech feed and
				for tries := 0; ; {
					if tries >= 2 {
						Logger.Warnf("Failed to fetch feed '%d' three time ... Retrying in next refresh cycle", feed.ID)
						break
					} else if cal, err := fetchFeed(feed.Url, feed.Type); err != nil {
						Logger.Warnf("Failed to fetch feed %d ... Retrying in 3 seconds", feed.ID)
						time.Sleep(time.Second * 3)
						tries++
					} else {
						// collect all used calendar ids to check later on if an event was removed
						eventIds := make([]string, 0)

						// process each event and detect if any updates were done or if an event is new
						for _, feedEvent := range cal.Events() {
							// marker if an update/ save is required
							modified := false

							// fetch or create event
							var calendarEvent Event

							if result := DB.Limit(1).Find(&calendarEvent, &Event{ID: feedEvent.Id()}); result.RowsAffected == 0 {
								calendarEvent = Event{ID: feedEvent.Id(), Serialized: feedEvent.Serialize(), CalendarID: calendar.ID}
								modified = true
							} else if result.Error != nil {
								Logger.Error("Failed to run event [%s] fetching query against the database: %s", feedEvent.Id(), err)
								continue
							} else {
								// compare serialized versions
								if serialized := feedEvent.Serialize(); calendarEvent.Serialized != serialized {
									calendarEvent.Serialized = serialized
									modified = true
								}
							}

							if calendarEvent.CalendarID != calendar.ID {
								Logger.Info("Event [%s] changed from calendar [%s] to calendar [%s]", feedEvent.Id, calendar.ID, calendarEvent.ID)
							}

							// handle start and end
							if start, err := feedEvent.GetStartAt(); err != nil {
								Logger.Warnf("Missed start field for event %s", feedEvent.Id())
							} else if start := start.UTC(); calendarEvent.Start != start {
								calendarEvent.Start = start
								modified = true
							}

							if end, err := feedEvent.GetEndAt(); err != nil {
								Logger.Warnf("Missed end field for event %s", feedEvent.Id())
							} else if end := end.UTC(); calendarEvent.End != end {
								calendarEvent.End = end
								modified = true
							}

							if modified {
								if err := DB.Save(&calendarEvent).Error; err != nil {
									Logger.Errorf("Failed to save calendar event: %s", err)
								}
							}

							eventIds = append(eventIds, calendarEvent.ID)
						}

						// delete all event ids that were not found
						if result := DB.Not(&eventIds).Delete(&Event{}); result.Error != nil {
							Logger.Errorf("Failed to cleanup calendar events: %s", err)
						} else if result.RowsAffected != 0 {
							Logger.Infof("Removed %d events from calendar [%s]", result.RowsAffected, calendar.Title)
						}

						calendar.LastRefresh = time.Now()
						if err := DB.Save(&calendar).Error; err != nil {
							Logger.Errorf("Failed to update LastRefresh for calendar [%s]: %s", calendar.Title, err.Error())
						}

						break
					}
				}
			}
		}

		Logger.Infof("Finished refreshing feeds. Sleeping for %s", timeout)

		time.Sleep(timeout)
	}
}
