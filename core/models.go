/*
Copyright © 2022 Cobalt <cobalt@cobalt.rocks>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package core

import (
	"fmt"
	"time"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type Calendar struct {
	ID          uint      `gorm:"primaryKey"`
	Title       string    `gorm:"not null;unique"`
	Description string    `gorm:"not null"`
	Url         string    `gorm:"not null;unique"`
	Type        string    `gorm:"not null"`
	LastRefresh time.Time `gorm:"not null;default:(datetime('now'))"`
	Events      []Event   `gorm:"foreignKey:CalendarID"`
}

type CalendarResponse struct {
	ID          uint
	Title       string
	Description string
}

type Event struct {
	ID         string    `gorm:"primaryKey"`
	Start      time.Time `gorm:"not null"`
	End        time.Time `gorm:"not null"`
	Serialized string    `gorm:"not null"`
	CalendarID uint      `gorm:"not null"`
}

var DB *gorm.DB

// open SQLite database from config, apply migrations and open pool
func LoadDatabase(database string) error {
	if db, err := gorm.Open(sqlite.Open(database), &gorm.Config{
		PrepareStmt: true,
	}); err != nil {
		return err
	} else {
		DB = db
		return SetupDatabase()
	}
}

// run migrations on the database
// TODO: Find a way to version and migrate over database scheme variants
func SetupDatabase() error {
	if err := DB.AutoMigrate(
		&Calendar{},
		&Event{},
	); err != nil {
		return fmt.Errorf("error while migrating database: %s", err)
	} else {
		return nil
	}
}
