package server

type ErrorResponse struct {
	Message string `json:"error"`
}

type EventResponse struct {
	ID         string
	Start      int64
	End        int64
	Serialized string
}
