/*
Copyright © 2022 Cobalt <cobalt@cobalt.rocks>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package server

import (
	"calx/core"

	"github.com/labstack/echo-contrib/prometheus"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// type alias for json
type JSON = map[string]interface{}

func SetupRouter() *echo.Echo {
	e := echo.New()

	/* Global middleware */
	// Zap logger middleware
	e.Use(core.LoggerMiddleware)

	// Recover Middleware
	e.Use(middleware.RecoverWithConfig(middleware.RecoverConfig{
		StackSize: 1 << 10, // 1 KB
	}))

	// optional middlewares
	if core.UseGZipMiddleware {
		e.Use(middleware.Gzip())
	}

	if core.UsePrometheusMiddleware {
		p := prometheus.NewPrometheus("calx", nil)
		p.Use(e)
	}

	// routes for handling event groups (calendars)
	calendars := e.Group("/calendars")

	calendars.GET("/:id", GetCalendar)
	calendars.GET("/list", ListCalendars)
	calendars.GET("/:id/list", ListCalendarEvents)

	// routes for handling individual events
	events := e.Group("/events")
	events.GET("/list", ListEvents)
	events.GET("/:id", GetEvent)

	// main feed
	e.GET("/feed", GetFeed)

	return e
}
