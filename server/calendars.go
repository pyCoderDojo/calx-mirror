/*
Copyright © 2022 Cobalt <cobalt@cobalt.rocks>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package server

import (
	"calx/core"
	"fmt"
	"math"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
)

// limits for results fetched via pagination
const paginationMaxPerPage = 30

// events may only be listed over the range of 3 months -- soft DOS protection
const queryMaxRange = 60 * 24 * 30 * 3

// fetch information about an individual calendar
func GetCalendar(c echo.Context) error {
	// fetch id from the url
	stringID := c.Param("id")

	// parse id
	id, err := strconv.ParseUint(stringID, 0, 64)

	if err != nil {
		c.JSON(http.StatusBadRequest, JSON{"error": "provided id was not a valid unsigned integer"})
	} else {
		// acquire connection to the database and search for calendar entry
		calendar := core.Calendar{ID: uint(id)}

		// fetch and check if result was found
		result := core.DB.Select("title", "description").Where(&calendar).First(&calendar)

		if result.Error != nil {
			c.JSON(http.StatusNotFound, JSON{"error": "No calendar with specified ID was found"})
		} else {
			c.JSON(http.StatusOK, JSON{"title": calendar.Title, "description": calendar.Description, "id": calendar.ID})
		}
	}

	return nil
}

// TODO: Implement routes

// fetch a list of all calendars
func ListCalendars(c echo.Context) error {
	var calendars []core.Calendar

	if result := core.DB.Select("id", "description", "title").Find(&calendars); result.Error != nil {
		return c.JSON(http.StatusInternalServerError, JSON{"error": "failed to fetch calendars"})

	} else {
		responses := make([]core.CalendarResponse, len(calendars))

		for idx, calendar := range calendars {
			responses[idx] = core.CalendarResponse{
				Title:       calendar.Title,
				Description: calendar.Description,
				ID:          calendar.ID,
			}
		}

		return c.JSON(http.StatusOK, responses)
	}
}

// fetch list of events in certain time span (from all calendars)
// parameters:
// start - unix timestamp (UTC) - default: NOW + 30 days
// end - unix timestamp (UTC) - default: NOW + 30 days
// page/ per-page: int/ int for pagination - 1/20
func ListEvents(c echo.Context) error {
	// extract parameters from the request
	page, per_page := uint16(0), uint16(0)
	start, end := int64(0), int64(0)

	if err := echo.QueryParamsBinder(c).
		Uint16("page", &page).
		Uint16("per_page", &per_page).
		Int64("start", &start).
		Int64("end", &end).
		BindError(); err != nil {
		return c.JSON(
			http.StatusBadRequest, ErrorResponse{Message: err.Error()},
		)
	}

	// check bounds for start and end
	if start == 0 {
		start = time.Now().UTC().Unix() - 60*24*30
	} else if end == 0 {
		start = time.Now().UTC().Unix() + 60*24*30
	} else if start > end {
		return c.JSON(
			http.StatusBadRequest, ErrorResponse{Message: "end needs to be greater than start"},
		)
	} else if start-end > queryMaxRange {
		return c.JSON(
			http.StatusBadRequest, ErrorResponse{Message: "The maximum range for [start, end] is 3 months"},
		)
	}

	// check pagination parameters
	if per_page > paginationMaxPerPage {
		return c.JSON(
			http.StatusBadRequest, ErrorResponse{Message: fmt.Sprint("per_page is limited to ", paginationMaxPerPage)},
		)
	} else if per_page == math.MaxUint16 || page == math.MaxUint16 {
		// primitive DOS defense
		return c.JSON(
			http.StatusBadRequest, ErrorResponse{Message: "per_page and page must to be lower than 2 ** 16"},
		)
	}

	// query for events
	events := make([]core.Event, 0)

	if err := core.DB.
		Select("serialized", "start", "end", "id").
		Offset(int(page)*int(per_page)).
		Limit(int(per_page)).
		Where("end <= ? AND start >= ?", time.Unix(end, 0), time.Unix(start, 0)).
		Find(&events).Error; err != nil {
		core.Logger.Error(err)
		return c.JSON(
			http.StatusInternalServerError, ErrorResponse{Message: "Failed to query for events. Error was logged"},
		)
	} else {
		responseEvents := make([]EventResponse, 0)

		for _, event := range events {
			responseEvents = append(responseEvents, EventResponse{
				Serialized: event.Serialized,
				ID:         event.ID,
				Start:      event.Start.Unix(),
				End:        event.End.Unix(),
			})
		}

		return c.JSON(http.StatusOK, responseEvents)
	}
}

// get information about a single event by its id
func GetEvent(c echo.Context) error {
	// extract id
	id := 0

	if err := echo.PathParamsBinder(c).
		MustInt("id", &id).
		BindError(); err != nil {
		return c.JSON(
			http.StatusBadRequest, ErrorResponse{Message: err.Error()},
		)
	}

	// query for event id
	var event core.Event

	// query with fallback for wither not found or for error while querying
	if result := core.DB.
		Select("serialized", "start", "end", "id").
		Where("id = ?", id).
		Limit(1).
		Find(&event); result.Error != nil {
		core.Logger.Error(result.Error)
		return c.JSON(
			http.StatusInternalServerError, ErrorResponse{Message: "Failed to query for event. Error was logged"},
		)
	} else if result.RowsAffected == 0 {
		return c.JSON(
			http.StatusNotFound, ErrorResponse{Message: "No entry with specified id was found"},
		)
	} else {
		return c.JSON(
			http.StatusOK, EventResponse{
				Serialized: event.Serialized,
				ID:         event.ID,
				Start:      event.Start.Unix(),
				End:        event.End.Unix(),
			},
		)
	}
}

// fetch information about all events of a calendar in certain date span with limit. Defaults to all events in the current month
func ListCalendarEvents(c echo.Context) error {
	return nil
}

// fet ical feed for calendar events
func GetFeed(c echo.Context) error {
	return nil
}
