/*
Copyright © 2022 Cobalt <cobalt@cobalt.rocks>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package server_test

import (
	"calx/core"
	"testing"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func FreshDb(t *testing.T) *gorm.DB {
	// setup helper
	t.Helper()

	// either open and apply migrations
	db, err := gorm.Open(sqlite.Open(":memory:"), &gorm.Config{})

	// fail directly on any error
	if err != nil {
		t.Fatalf("Error opening in-memory db: %s", err)
	} else {
		core.DB = db
	}

	if err := core.SetupDatabase(); err != nil {
		t.Fatalf("Error setting up db: %s", err)
	}

	return db
}
