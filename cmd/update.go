/*
Copyright © 2022 Cobalt <cobalt@cobalt.rocks>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"calx/core"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update calendar feeds in database",
	Long:  "Calendar feeds are read from the feed file and checked against the database entries",
	Run: func(cmd *cobra.Command, args []string) {
		// init config and database
		core.InitConfig()

		// open and setup the database
		if dbErr := core.LoadDatabase(viper.GetString("database")); dbErr != nil {
			core.Logger.Fatal(dbErr)
		}

		// register feeds
		core.RegisterFeeds(*dryRun)

		// sync up logger
		core.Logger.Sync()

		os.Exit(0)
	},
}

var dryRun *bool

func init() {
	// setup command
	rootCmd.AddCommand(updateCmd)
	dryRun = updateCmd.Flags().BoolP("dry", "d", false, "Dry run - Don't apply changes")
}
