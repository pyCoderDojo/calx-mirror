/*
Copyright © 2022 Cobalt <cobalt@cobalt.rocks>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"calx/core"
	"calx/server"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start the web server",
	Run: func(cmd *cobra.Command, args []string) {
		core.InitConfig()

		// open and setup the database
		if dbErr := core.LoadDatabase(viper.GetString("database")); dbErr != nil {
			core.Logger.Fatal(dbErr)
		}

		// register feeds
		core.RegisterFeeds(false)

		// seperated feed fetching
		go core.FetchFeeds()

		// setup router and start server
		err := server.SetupRouter().Start(fmt.Sprintf("%s:%d", viper.GetString("host"), viper.GetUint("port")))

		if err != nil {
			core.Logger.Fatal(err)
		} else {
			core.Logger.Sync()
			os.Exit(0)
		}
	},
}

func init() {
	// setup command
	rootCmd.AddCommand(serveCmd)
}
