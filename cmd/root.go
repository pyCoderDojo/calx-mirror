/*
Copyright © 2022 Cobalt <cobalt@cobalt.rocks>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"calx/core"
	"calx/server"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "calx",
	Short: "Small callendar server for organizing coderdojo events and broadcasting other events",
	Long:  `Long description of your application`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()

	if err != nil {
		os.Exit(1)
	}
}

// init configures the viper configuration options and rootCmd Flags
func init() {
	// setup configuration
	viper.SetEnvPrefix("CALX_")
	viper.SetDefault("port", 3000)
	viper.SetDefault("refresh", 3600)
	viper.SetDefault("mode", "development")
	viper.SetDefault("host", "localhost")
	viper.SetDefault("database", "calx.db")
	viper.SetDefault("feeds", server.JSON{})
	viper.SetDefault("gzip", false)
	viper.SetDefault("jaeger", false)
	viper.SetDefault("prometheus", false)

	// setup global flags
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	rootCmd.PersistentFlags().StringVar(&core.CfgFile, "config", "config.json", "config file (default is config.json)")
	core.IgnoreConfig = rootCmd.PersistentFlags().Bool("ignore-config", false, "Ignore config file")
}
