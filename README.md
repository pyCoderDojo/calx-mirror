[![License](https://img.shields.io/badge/License-AGPL-red?logo=gnu)](https://www.gnu.org/licenses/agpl-3.0.en.html)

# CALX

An iCal feed aggregator with filtering capabilities. Developed for the Coderdojo R & B by [Cobalt](https://cobalt.rocks) and built with [echo](https://echo.labstack.com/). Only supported for GNU/Linux.

Repository Icon from [Phosphor Icons](https://github.com/phosphor-icons/phosphor-home) by [Phosphor Icons](https://github.com/phosphor-icons/phosphor-home/blob/master/LICENSE) under the MIT license

## Goals

> Note: The server is http-only and should only be used behing a TLS-terminating reverse proxy

- aggregate iCal calendars and expose over filterable interface
- dead simple configuration for feeds

## Development

You need a [Go Compiler](https://go.dev/) as well as a CGO-supported C-compiler (like GCC or Clang/LLVM) (for sqlite).

- Starting with auto-reload (requires [air](https://github.com/cosmtrek/air)): `air run`
- Compile to a single binary: `go build`
- Test all routes: `go test`

## Configuration

Configuration is done over either a config file (default: `config.json`) or environment variables. Refer to the table below for configuration options.

| Name         | Description                                                                                                                                                                                                               | Type                  | Default       | Environment       |
| ------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- | ------------- | ----------------- |
| `jeager`     | Enables support for reporting server activity to jeager. Refer to the echo docs for [specific configuration options of the employed middleware](https://echo.labstack.com/middleware/jaegertracing/#custom-configuration) | `boolean`             | `false`       | `CALX_JEAGER`     |
| `gzip`       | Enables support for gzip response compression                                                                                                                                                                             | `boolean`             | `false`       | `CALX_GZIP`       |
| `prometheus` | Enables support for prometheus application metrics at `/metrics`                                                                                                                                                          | `boolean`             | `false`       | `CALX_PROMETHEUS` |
| `feeds`      | Specifies feeds that should be offered by the server. For the full structure see below.                                                                                                                                   | `map[string]Feed`     | `{}`          | `CALX_FEEDS`      |
| `refresh`    | Specifies refresh interval for feeds in seconds                                                                                                                                                                           | `number`              | `3600`        | `CALX_REFRESH`    |
| `host`       | Specifies host for HTTP server                                                                                                                                                                                            | `string` (fqdn or ip) | `localhost`   | `CALX_HOST`       |
| `port`       | Specifies host for HTTP server                                                                                                                                                                                            | `uint16`              | `3000`        | `CALX_PORT`       |
| `database`   | Specifies location for SQLite the database                                                                                                                                                                                | `string` (path)       | `calx.db`     | `CALX_DATABASE`   |
| `mode`       | Specifies logger mode: `production` = structured logging, `development` = pretty logging (default fallback)                                                                                                               | `string`              | `development` | `CALX_MODE`       |

### Feeds

> At the moment only iCal feeds are supported. ics support is planned for future

Feeds consist of a title, description and URL (source for the iCal feed). Addionally a `type` field specifies if the URL is referring to a web feed (`"type": "web"`) or to a local file (`"type": "file"`). This can be used to specify additional events from local files and will in the future also be used to support ics files.

The feeds field in the configuration contains a dictionary with the titles serving as keys and description and url being the associated values.

For example:

```json
{
  "GPS Affiliate Anniversary stream": {
    "url": "https://horaro.org/gps-1/schedule.ical",
    "description": "Schedule for the GPS Annual Affiliate Anniversary stream",
    "type": "web"
  }
}
```

> All feeds are tracked in a SQLite and registered/ updated on server startup. <br />
> You can also manually call the `update` subcommand to update the database
